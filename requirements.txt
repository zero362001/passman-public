setuptools~=57.4.0
requests~=2.26.0
pathlib==1.0.1
tenacity>=5.1.5,<6.0.0
pycryptodome>=3.9.6,<4.0.0
keyring
pyperclip