import os
import string
import random
import time
from cipher import Cipher
from Crypto import Random
from serialiser import Serialiser


class Manager(object):

    def __init__(self, hash_path, data_path, cloud_handle=None):
        self.hash_path = hash_path
        self.data_path = data_path
        self.cloud_handle = cloud_handle
        self.spec_char = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '?', ]
        self.key = None
        self.data = None
        self.changed = False

    def reset_master_password(self, key):
        self.changed = True
        _hash = Cipher.hash_pass(key)
        self.key = key.encode()
        with open(self.hash_path, 'w') as h_file, open(self.data_path, 'wb') as d_file:
            h_file.write(_hash)
            salt = Random.new().read(16)
            d_file.write(salt)
        time_modified = time.mktime(time.localtime())
        if self.cloud_handle:
            self.cloud_handle.save(self.hash_path, str(time_modified))
        parent_dir = os.path.abspath(os.path.join(self.data_path, os.pardir))
        with open(os.path.join(parent_dir, 'time_modified'), 'w') as file:
            file.write(f'{time_modified}')
        return True

    def verify_creds(self, key: str) -> int:
        stat = 0  # -1: No key, 1: Match, 0: Fail
        if os.path.exists(self.hash_path):
            with open(self.hash_path, 'r') as _file:
                _hash = _file.read()
                if Cipher.verify_pass(_hash, key.encode()):
                    stat = 1
                    self.key = key.encode()
        else:
            stat = -1

        return stat

    def load(self):
        if self.cloud_handle:
            last_modified = 0
            parent_dir = os.path.abspath(os.path.join(self.data_path, os.pardir))
            if os.path.exists(os.path.join(parent_dir, 'time_modified')):
                with open(os.path.join(parent_dir, 'time_modified'), 'r') as file:
                    last_modified = file.read().replace('/n', '')
                    try:
                        last_modified = int(last_modified)
                    except ValueError:
                        last_modified = 0
            sync_status = self.cloud_handle.is_later(last_modified)
            if sync_status == 2:
                self.cloud_handle.load('pman.dat', self.data_path)
            elif sync_status in (-1, 1):
                self.cloud_handle.save(self.data_path, os.path.join(parent_dir, 'time_modified'))
                self.cloud_handle.save(self.hash_path)
        ser = Serialiser(self.data_path)
        self.data = ser.load(self.key)

    def save(self):
        if not self.changed:
            return True
        time_modified = time.mktime(time.localtime())
        parent_dir = os.path.abspath(os.path.join(self.data_path, os.pardir))
        with open(os.path.join(parent_dir, 'time_modified'), 'w') as file:
            file.write(f'{time_modified}')
        ser = Serialiser(self.data_path)
        stat = ser.dump(data=self.data, key=self.key)
        if self.cloud_handle:
            self.cloud_handle.save(self.data_path, os.path.join(parent_dir, 'time_modified'))
        return stat

    def add_entry(self, val, force=True):
        sid = val[0]
        pwd = val[1]
        if sid in self.data:
            if force:
                self.changed = True
                self.data[sid] = pwd
                return True
            else:
                return False
        else:
            self.changed = True
            self.data[sid] = pwd
            return True

    def rem_entry(self, sid):
        self.changed = True
        self.data.pop(sid)

    def clear_data(self):
        self.changed = True
        self.data.clear()

    def get_data(self):
        return self.data

    def has_entry(self, sid):
        return sid in self.data

    def get_entry(self, sid):
        return self.data[sid]

    @staticmethod
    def generate_random(char_set, length=12) -> str:
        key = ''
        if length not in range(12, 25):
            length = 12

        while not Manager.validate_key(char_set, key):
            key = ''
            for i in range(length):
                key += random.SystemRandom().choice(string.ascii_letters + string.digits + char_set)

        return key

    @staticmethod
    def validate_key(char_set, key: str) -> bool:
        flags = 0
        if len(key) < 12:
            return False
        for c in key:
            if ord(c) in range(65, 91):
                flags |= 2
            elif ord(c) in range(97, 123):
                flags |= 4
            elif ord(c) in range(48, 58):
                flags |= 8
            elif c in char_set:
                flags |= 16
            elif ord(c) == 32:
                return False

        return flags == 30
