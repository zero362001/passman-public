import os
import json
from cipher import Cipher


class Serialiser(object):

    def __init__(self, filename):
        self.filename = filename
        with open(filename, 'rb') as dfile:
            self.psalt = dfile.read(16)

    def set_salt(self, salt: bytes):
        self.psalt = salt

    def dump(self, key: bytes, data: dict) -> bool:
        if len(data) == 0:
            return True

        json_dat = json.dumps(data).encode()
        encryptor = Cipher(key, self.psalt)
        enc_dat = encryptor.encrypt(json_dat)

        with open(self.filename, 'wb') as _file:
            _file.write(self.psalt)
            _file.write(enc_dat)
            _file.close()

        return True

    def load(self, key: bytes) -> dict:
        _dict = {}

        with open(self.filename, 'rb') as _file:
            # self.psalt = _file.read(16)
            _file.read(16)
            cipher = Cipher(key, self.psalt)
            data = _file.read()
            if len(data) > 0:
                dec = cipher.decrypt(data)
                _dict = json.loads(dec)

        return _dict
