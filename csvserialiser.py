import csv


def import_db(filename):
    data = {}
    try:
        with open(filename, 'r') as file:
            reader = csv.reader(file)
            pword_col, uname_col, service_col = -1, -1, -1
            col_n, row_n = 0, 0
            for row in reader:
                if row_n == 0:
                    for col in row:
                        if 'username' in col and uname_col == -1:
                            uname_col = col_n
                        elif 'url' in col and service_col == -1:
                            service_col = col_n
                        elif 'title' in col and service_col == -1:
                            service_col = col_n
                        elif 'password' in col and pword_col == -1:
                            pword_col = col_n
                        col_n += 1
                else:
                    sid = f'{row[uname_col]}@{row[service_col]}'
                    data[sid] = row[pword_col]
                row_n += 1
        return data
    except IOError as err:
        print('Could not import passwords! Error opening file!')


def export_db(filename, data):
    try:
        with open(filename, 'w') as file:
            writer = csv.writer(file)
            writer.writerow(['url', 'username', 'password'])
            for entry in data:
                tokens = entry.split('@')
                username = '@'.join(tokens[0:-1])
                service = tokens[-1]
                password = data[entry]
                writer.writerow([service, username, password])
            return 0
    except IOError:
        return -1
